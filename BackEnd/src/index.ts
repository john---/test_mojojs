import {EventEmitter} from 'node:events'
import mojo, {yamlConfigPlugin} from '@mojojs/core'
import Pg from '@mojojs/pg'
import Path from '@mojojs/path'
import { DbMessages } from './models/dbmessages.js'
import { FeatureMsg, Feature, featureMap, AudioMsg } from './models/types.js'

export const app = mojo()

app.models.events = new EventEmitter()
app.models.events.setMaxListeners(1000)

app.plugin(yamlConfigPlugin)
app.secrets = app.config.secrets

app.onStart(async app => {
  if (app.models.pg === undefined) app.models.pg = new Pg(app.config.pg)  // check is for test overrides (not currently using)
  app.models.dbmessages = new DbMessages(app.models.pg, app.log, app.models.events, Array.from(Object.keys(featureMap)) as Array<Feature>)

  let feature: keyof typeof featureMap
  for (feature in featureMap) {
      let featureClass = featureMap[feature];
      app.models[feature] = new (featureClass)(app.models.pg, app.config, app.log, app.models.events)
      app.models[feature].start()
  }
})

app.onStop(async app => {
  app.models.dbmessages.close()

  let feature: keyof typeof featureMap
  for (feature in featureMap) {
      app.log.debug(`closing feature: ${feature}`)
      await app.models[feature].close()
  }


})

app.get('/', async ctx => {

    await ctx.sendFile(ctx.home.child('public', 'index.html'))

    // // Testing user authentication

    // if (ctx.req.userinfo && process.env.USERS?.includes(ctx.req.userinfo)) {
    //   await ctx.sendFile(ctx.home.child('public', 'index.html'))
    // }

    // // Require authentication
    // ctx.res.set('WWW-Authenticate', 'Basic');
    // return ctx.render({text: 'Authentication required!', status: 401});
})

app.get('/audio', async ctx => {

  const params = await ctx.params()
  const file = params.get('file')
  const source = params.get('source')

  // rewrite this to get a transmission ID and then use
  // database to retreive file name and directory (/tmp vs. /cart/data/wav)
  // this route is currently generic across all features
  await ctx.sendFile(new Path(`${app.config.audio_dir[source!]}${file}`))
})

app.websocket('/messages', async ctx => {

    ctx.json(async ws => {

      const params = await ctx.params()
      const feature = params.get('feature')

      // valid features only
      if (feature == null) {
        app.log.error('feature not supplied')
        ws.close(undefined, 'no feature supplied')
        return
      } else if (!featureMap[feature]) {
        app.log.error(`feature not valid: ${feature}`)
        ws.close(undefined, 'feature not valid')
        return
      }

      // get things to the user
      const sendMsg = (msg: FeatureMsg | AudioMsg) => ws.send(msg) 
      
      // messages that come in from pub/sub and other aysynchronous paths
      ctx.models.events.on(feature, sendMsg) // only need one of these per feature across all clients

      await app.models[feature].userConnected?.(ctx.req.requestId)

      // // pass user info to feature (user auth is in testing)
      // if (ctx.req.userinfo) {
      //   await app.models[feature].userConnected?.(ctx.req.requestId)
      //   // await app.models[feature].userConnected?.(ctx.req.userinfo.split(':')[0])
      // } else {
      //   // TODO: handle no user info
      //   ctx.log.error('user not logged in')
      // }

      // handle messages from the client
      for await (const data of ws as unknown as Array<FeatureMsg>) {
        app.models[feature].handleMsg(data)
          .then((res: FeatureMsg) => {
            sendMsg(res)
          })
          .catch((err: FeatureMsg) => {
            ctx.log.error(`msg: ${JSON.stringify(data)} ${JSON.stringify(err)}`)
            sendMsg(err)
          })
      }

      //await app.models[feature].close()

    })
  })
  
  
app.start();
