import { Logger } from "@mojojs/core/lib/logger"
import { EventEmitter } from 'node:events'
import Sensor from "./telemetry/sensor"

export default class ThermalControl {

  log: Logger
  events: EventEmitter
  fans: Array<Sensor>
  config: Record<string, any>
  lastChangedAt: number

  constructor(log: Logger, events: EventEmitter, fans: Array<Sensor>, config: Record<string, any>) {

    this.log = log
    this.events = events
    this.fans = fans
    this.config = config
    this.lastChangedAt = -100

    events.on('telemetry', this.handleCpuTemp)
  }

  handleCpuTemp = (msg: any) => {
    if (msg.type != 'update') { return }

    const getProposedFanSpeed = (temp: number) => {
      // scale cpu temp to fan speed

      const tempRange = this.config.telemetry.fan_control.temp
      const speedRange = this.config.telemetry.fan_control.speed

      var fanSpeed = Math.round((temp - tempRange.min) * (speedRange.max - speedRange.min) / (tempRange.max - tempRange.min) + speedRange.min)
  
      fanSpeed = fanSpeed < speedRange.min ? speedRange.min : fanSpeed
      fanSpeed = fanSpeed > speedRange.max ? speedRange.max : fanSpeed
  
      return fanSpeed
    }

    msg.metrics.forEach(async (item: any) => {
      if (item.input === 'cpu_temp') {
        if (Math.abs(item.value - this.lastChangedAt) >= 2) {
          const speed = getProposedFanSpeed(item.value)
          this.fans.forEach((sensor) => sensor.setFanSpeed(speed))
          this.lastChangedAt = item.value
        }
      }
    })

  }

  async close() {
    this.events.removeListener('telemetry', this.handleCpuTemp)
  }

}