import Pg from "@mojojs/pg"
import { EventEmitter } from 'node:events'
import { Logger } from "@mojojs/core/lib/logger"
import { exec } from 'child_process'
import util from "node:util"
import { AdministrationMsg } from './types.js'

export class Administration {

    pg: Pg
    config: Record<string, any>
    log: Logger
    source: string
    events: EventEmitter
    
  constructor(pg: Pg, config: Record<string, any>, log: Logger, events: EventEmitter) {

    this.pg = pg
    this.config = config
    this.log = log
    this.source = 'administration'
    this.events = events
  }

  start () {
    this.log.info('starting administration')
  }

  async handleMsg(msg: AdministrationMsg) {
    return new Promise(async (resolve, reject) => {

      switch (msg.type) {
        case 'status':
          // maybe some logic to better confirm being green
          resolve({ type:"status", detail:"green" })
          break
        case 'config':
          resolve(await this.getConfig())
          break
        case 'command':
          resolve(await this.executeCommand(msg.command))
        default:
          reject({ type:'error', detail:`invalid type: ${msg.type}`})
      }
    })
  }

  async getConfig() {
    const systemCommands = await this.getSystemCommands()
    const config = { type: 'config', system: systemCommands }

    return config
  }

  async getSystemCommands() {
    return Object.keys(this.config.admin.system)
  }

  async executeCommand(command: string) {

    const execPromise = util.promisify(exec);

    try {
      // wait for exec to complete
      const {stdout} = await execPromise(this.config.admin.system[command])
      this.log.debug(`stdout: ${stdout}`)
      return ( { type:'status', detail:'green' } )
    } catch (error) {
      this.log.error(error as string)
      return ( {type:'error', detail:`error executing command: ${error}`})
    }
  }

  async close() {
  }

}