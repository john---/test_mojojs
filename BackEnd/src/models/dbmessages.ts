/*
 * Subscribe to messages and if source matches send them to client notifier
 * These are the messages that come in from external sources via database pub/sub
*/

import { Logger } from "@mojojs/core"
import Pg, { Database } from "@mojojs/pg"
import { EventEmitter } from 'node:events'
import { AudioMsg, Feature } from "./types"

export class DbMessages {

  pg: Pg
  db: Database | undefined
  log: Logger
  sources: Array<Feature>
  events: EventEmitter

  constructor(pg: Pg, log: Logger, events: EventEmitter, sources: Array<Feature>) {
    this.pg = pg
    this.log = log
    this.sources = sources
    this.events = events

    this.pg.db().then(
      (result) => {
        this.db = result
        for (const source of this.sources) {
          this.db.listen(source)
        }
        this.db.on('notification', (message: any) => {
          const msg: AudioMsg = JSON.parse(message.payload)
          if (msg.type === 'audio') {
            this.events.emit(message.channel, msg)
          }
        });
        // setInterval(() => {
        //    this.db?.notify('scanner', '{"scanner": "test"}')   // test message
        // }, 5000)
      },
      (error) => { this.log.error(error) }
    )

    pg.query`SELECT version()`.then(
      (results) => { log.info(results[0].version) },
      (error) => log.error(error)
    )
  }

  async close() {
    if (typeof this.db !== 'undefined') {
      for (const source of this.sources) {
        this.db.unlisten(source)
      }
      this.db.release()
    }
  }
}