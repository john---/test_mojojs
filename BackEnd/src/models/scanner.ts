import Pg, { Results } from "@mojojs/pg"
import { Logger } from "@mojojs/core/lib/logger"
import { ScannerMsg, ScannerChangeMsg, ScannerUpdateMsg, Bank } from './types.js'
import { DatabaseError } from "pg-protocol"

export class Scanner {

    pg: Pg
    config: Record<string, any>
    log: Logger
    source: string
    
  constructor(pg: Pg, config: Record<string, any>, log: Logger) {

    this.pg = pg
    this.config = config
    this.log = log
    this.source = 'scanner'
  }

  start() {
    this.log.info('starting scanner')
  }

  // async waitFor(i: number) {
  //   return new Promise ((resolve, reject) => {
  //     setTimeout(() => {
  //       resolve(i)
  //     }, 1000 * i)
  //   })
  // }

  async handleMsg(msg: ScannerMsg) {
    return new Promise(async (resolve, reject) => {

      switch (msg.type) {
        case 'status':
          // maybe some logic to better confirm being green
          resolve({ type:"status", detail:"green" })
          break
        case 'change':
          resolve(await this.handleChange(msg))
          break
        case 'config':
          resolve(await this.getConfig())
          break
        default:
          reject({ type:'error', detail:`invalid type: ${msg.type}`})
      }
    })
  }

  async handleChange(msg: ScannerChangeMsg) {

    if (!('value' in msg)) { return { type: 'error', detail: 'No "value" property' } }
 
    const regexp = /^[A-Z0-9-_ \(\)}]+$/gi
    if (!regexp.test(msg.value)) { 
      return { type: 'error', detail: 'Invalid character in field' }
    }

    try {
      const update = await this.updateFreqs(msg)
      let resp: ScannerUpdateMsg = msg as any
      if (update.count) {
        resp.type = 'update'
        return resp
      } else {
        return ( {type: 'error', detail: `could not update xmit: ${resp.xmit_key}`})
      }
    } catch (error: any) {
      if (error instanceof DatabaseError) {
        return ( {type: 'error', detail: error.detail })
      } else {
        return ( {type: 'error', detail: error })
      }
    }
  }

  updateFreqs(msg: ScannerChangeMsg) {
    
    let results: Promise<Results<any>>
    switch (msg.field) {
      case 'label':
        results = this.pg.rawQuery('update freqs set label = $1 where freq_key = $2', msg.value, msg.freq_key)
      break
      case 'bank':
        results = this.pg.rawQuery('update freqs set bank = $1 where freq_key = $2', msg.value, msg.freq_key)
        break
      case 'class':
        results = this.pg.rawQuery('update xmit_history set class = $1 where xmit_key = $2', msg.value, msg.xmit_key)
      break
      default:
        throw `field cannot be changed: ${msg.field}`
    }

    return results
  }

  async getConfig() {
    const banks = await this.getBanks()
    const config = { type: 'config', banks: banks }

    return config
  }
  
  async getBanks() {
    const results = this.pg.query<Bank>`select distinct(bank) from freqs order by bank asc`
    .then( (banks) => { return banks.map( (item) => { return item.bank })})
  
    return results
  }

  async close() {
    // nothing be handled yet
  }

}