import Pg from "@mojojs/pg"
import { EventEmitter } from 'node:events'
import { Logger } from "@mojojs/core/lib/logger"
import { TelemetryMsg } from './types.js'
import { Factory } from './telemetry/sensor_factory.js'
import ThermalControl from "./ThermalControl.js"
import { setSubscription } from './telemetry/sensor.js'

export class Telemetry {

    pg: Pg
    config: Record<string, any>
    log: Logger
    source: string
    events: EventEmitter
    i2cCheck: any
    i2c: any
    sensors: any
    thermControl: ThermalControl | undefined
    //subscribed: boolean
    
  constructor(pg: Pg, config: Record<string, any>, log: Logger, events: EventEmitter) {

    this.pg = pg
    this.config = config
    this.log = log
    this.source = 'telemetry'
    this.events = events
    this.i2cCheck
    this.sensors = []
    //this.subscribed = false

    // instantiate Sensors
    for (const a_config of config.telemetry.sensors) {
      this.sensors.push(Factory.getSensor(a_config, config.telemetry.volume, this.source, pg, events, log))
    }

    this.startThermalControl()
  }

  start () {
    this.log.info('starting telemetry')
    for (const sensor of this.sensors) {
      sensor.kickoff()  // start the sensor's loop
    }
  }

  async userConnected(requestID: string) {
    this.log.debug(`request ID of connection: ${requestID}`)

    // emit min/max for sensors
    let metrics = []
    for (const sensor of this.sensors) {
      const minMax = await sensor.getMinMax()
      if (minMax) { metrics.push(minMax) }
    }
    if (metrics) {
      this.events.emit(this.source, { type: 'update', metrics: metrics })
    }
  
    setSubscription(true)
  }

  async handleMsg(msg: TelemetryMsg) {
    return new Promise(async (resolve, reject) => {

      switch (msg.type) {
        case 'status':
          // maybe some logic to better confirm being green
          resolve({ type:"status", detail:"green" })
          break
        case 'config':
          resolve({ type:"config"})
          //resolve(await this.getConfig())
          break
        case 'subscribe':
          //this.subscribed = true
          setSubscription(true)
          resolve({ type:"subscribe"})
          //resolve(await this.getConfig())
          break
        case 'unsubscribe':
          setSubscription(false)
          //this.subscribed = false
          resolve({ type:"unsubscribe"})
          //resolve(await this.getConfig())
          break
        default:
          reject({ type:'error', detail:`invalid type: ${msg.type}`})
      }
    })
  }

  // read temp and control fans to cool things down
  startThermalControl() {
    const fans = this.sensors.filter((sensor: any) => sensor.isFan())
    this.thermControl = new ThermalControl(this.log, this.events, fans, this.config)
  }

  async close() {
    for (const sensor of this.sensors) {
      await sensor.close()
    }
    await this.thermControl?.close()
  }

}