
import { openPromisified } from 'i2c-bus'
import Sensor from './sensor.js'
import { Logger } from '@mojojs/core'
import Pg from '@mojojs/pg'
import { EventEmitter } from 'node:stream'

const REG: {[index: string]:any} = {
  TACH: {
    FAN1: {
        LOWBYTE: 0x2A,
        HIGHBYTE: 0x2B
    },
    FAN2: {
        LOWBYTE: 0x2C,
        HIGHBYTE: 0x2D
    },
    FAN3: {
        LOWBYTE: 0x2E,
        HIGHBYTE: 0x2F
    },
    FAN4: {
        LOWBYTE: 0x30,
        HIGHBYTE: 0x31
    }
  },
  DUTY: {
    FAN1: 0x32,
    FAN2: 0x33,
    FAN3: 0x34,
    FAN4: 0x35
},
  DEVICEID: 0x3D,    // R
  COMPANYID: 0x3E,   // R
  REVNUMBER: 0x3F,   // R
  CONFIG1: 0x40,     // R/W
}

const STALLED = 0xFFFF

export default class ADT7470 extends Sensor {

  constructor (config: any, volume: number, source: string, pg: Pg, events: EventEmitter<[never]>, log: Logger) {

    super(config, volume, source, pg, events, log)
  }

  /**
   * Concrete implementation of the abstract method.
   * @param name The name of the sensor.
   * @returns raw value from fan (speed in rpm)
   */
  async readRaw(name: string): Promise<number | undefined> {
    if (name == 'fan1') {
      return this.readFanRpm(1)
    } else if (name == 'fan2') {
      return this.readFanRpm(2)
    }
  }

  /**
   * Concrete implementation of the abstract method.
   * @returns delay between reads (seconds)
   */
  async getDelay() {
    return this.config.rate
  }

  /**
   * Returns the fan speed in RPM.
   *
   * @returns The fan speed in RPM
   *
   */
  async readFanRpm(fan: number) {

    const bus = await openPromisified(this.config.params.bus)
    if (bus) {
      const lo = await bus.readWord(this.config.params.addr, REG.TACH[`FAN${fan}`].LOWBYTE)
      const hi = await bus.readWord(this.config.params.addr, REG.TACH[`FAN${fan}`].HIGHBYTE)
      bus.close()
      if (lo && hi) {
        //const raw = (hi<<8) + lo
        // const raw = (hi << 4) | (lo >> 5)
        const raw = (hi & 0xff) << 8  | (lo & 0xff)
        const rpm = (raw != STALLED) ? Math.round((90000*60)/raw) : 0

        return rpm
      }
    }
  }

  // async readFan1Rpm() {
  //   return this.readFanRpm(1)
  // }

  // async readFan2Rpm() {
  //   return this.readFanRpm(2)
  // }

  /**
   * Writes the pwm duty cycle as a percentage for the specified fan.
   *
   */
    async writeDuty(fan: number, duty: number) {
    
      //$fan = $self->_format_fan($fan);
 
      if ( duty < 0 )   { duty = 0 }
      if ( duty > 255 ) { duty = 255 }

      const bus = await openPromisified(this.config.params.bus)
      if (bus) {
        // const buffer = new ArrayBuffer(2)
        // const int16View = new Int16Array(buffer)
        // int16View[0] = duty        
        const buffer = new ArrayBuffer(2)
        const int8View = new Uint8Array(buffer)
        int8View[0] = duty
        await bus.writeByte(this.config.params.addr, REG.DUTY[`FAN${fan}`], int8View[0])
        bus.close()
      }

    }

  /**
   * Writes the pwm duty cycle as a percentage for the specified fan.
   *
   */
  async writeDutyPercent(fan: number, percent: number) {

    this.writeDuty( fan, percent / 100 * 255 );
  }

  async setFanSpeed(fanSpeed: number) : Promise<void> {
    this.log.debug(`setting ${this.config.name} speed to ${fanSpeed}%`)
    const fan = Number(this.config.name.replace('fan', ''))
    await this.writeDutyPercent(fan, fanSpeed)

  }

}