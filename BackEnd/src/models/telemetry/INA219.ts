
import { openPromisified } from 'i2c-bus'
import Sensor from './sensor.js'
import { Logger } from '@mojojs/core'
import Pg from '@mojojs/pg'
import { EventEmitter } from 'node:stream'

const REG = {
  CONFIG: 0x00,  // R/W
  VSHUNT: 0x01,  // R
  VBUS: 0x02,    // R
  POWER: 0x03,   // R
  CURRENT: 0x04, // R
  CALIB: 0x05,   // R/W
}

export default class INA219 extends Sensor {

  constructor (config: any, volume: number, source: string, pg: Pg, events: EventEmitter<[never]>, log: Logger) {

    super(config, volume, source, pg, events, log)
  }

  /**
   * Concrete implementation of the abstract method.
   * @param name The name of the sensor.
   * @returns raw value from fan (speed in rpm)
   */
  async readRaw(name: string): Promise<number | undefined> {
    if (name == 'voltage') {
      return this.readVoltage()
    } else if (name == 'current') {
      return this.readCurrent()
    }
  }

  /**
   * Concrete implementation of the abstract method.
   * @returns delay between reads (seconds)
   */
  async getDelay() {
    return this.config.rate
  }

  /**
   * Returns the current shunt voltage reading scaled integer in microvolts.
   *
   * @returns The shunt voltage in uV
   *
   */
  async readShuntVoltage() {

    const toVshunt = (rawData: number) => {
      const beData = (rawData >> 8) + ((rawData & 0xff) << 8)
      const vShunt = beData * 10.0

      return vShunt
    }

    const bus = await openPromisified(this.config.params.bus)
    if (bus) {
      const i2c0 = await bus.readWord(this.config.params.addr, REG.VSHUNT)
      bus.close()
      if (i2c0) {
        const vShunt = toVshunt(i2c0)
        return vShunt
      }
    }
  }

  /**
   * Returns the current in millamps.
   *
   * @returns The current in mA
   *
   */
  async readCurrent() {
    const shuntVoltage = await this.readShuntVoltage()
    // 0.1 Ohm resistor.  TODO: config ina219 and use current register
    return shuntVoltage! / 0.1 / 1000
  }

    /**
   * Returns the bus voltage reading scaled integer in millivolts and other relevant info.
   *
   * @returns The shunt voltage in mV and OVF, CNVR flags
   *
   */  
  async readBusVoltage() {

    const toVbus = (rawData: number) => {
      const beData = (rawData >> 8) + ((rawData & 0xff) << 8)

      const ovf  = ( beData & 1<<0 )
      const cnvr = ( beData & 1<<1 )
      const vraw = beData >> 3

      return [vraw * 4, cnvr, ovf]
    }

    const bus = await openPromisified(this.config.params.bus)
    if (bus) {
      const i2c0 = await bus.readWord(this.config.params.addr, REG.VBUS)
      bus.close()
      if (i2c0) {
        const values = toVbus(i2c0)
        return values
      }
    }

  }

  /**
   * Returns the bus voltage reading scaled integer in volts.
   *
   * @returns The shunt voltage in mV
   *
   */  
  async readVoltage() {
    const busVoltage = await this.readBusVoltage()
    return busVoltage![0] / 1000
  }

}