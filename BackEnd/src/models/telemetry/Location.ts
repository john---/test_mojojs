import gpsd from 'node-gpsd'
import { Logger } from "@mojojs/core"
import { setTimeout } from "node:timers/promises"
import fs from 'fs/promises'
import Sensor from './sensor.js'
import Pg from '@mojojs/pg'
import { EventEmitter } from 'node:stream'

/**
 * This class is responsible for both the recording of location data
 * and the reporting of the current speed.
 * 
 * Reporting of speed and writing of location data is
 * done at the rate in config.yaml.  However, if the
 * speed is greater than a specific number the rate is
 * increased.
 * 
 */
export default class Location extends Sensor {

  listener: any
  location: any | undefined
  file: string
  delayMaxSpeedReport: any
  closing: boolean

  constructor (config: any, volume: number, source: string, pg: Pg, events: EventEmitter<[never]>, log: Logger) {

    super(config, volume, source, pg, events, log)

    this.location = undefined
    this.closing = false

    const d_t = new Date()
    const year = d_t.getFullYear()
    const month = ("0" +(d_t.getMonth() + 1)).slice(-2)
    const day = ("0" + d_t.getDate()).slice(-2)
    this.file = `${config.params.gps_dir}/${year}${month}${day}.gps`

    this.listener = new gpsd.Listener({
      hostname: this.config.params.hostname,
      port: this.config.params.port,
      parse: true
    })

    this.listener.logger = console

    this.connectToGps()

    this.listener.on('TPV', (data: any) => this.handleGpsData(data))

    this.listener.on('disconnected', (data: any) => {
      if (this.closing) {
        return
      }
    
      this.log.error('disconnected from gpsd!')
      
      this.location = undefined
    
      setTimeout(10 * 1000).then(async () => {
        this.connectToGps()
      })
    })

    this.listener.on('DEVICE', (device: any) => {
      this.log.debug(`Gps device: ${device.subtype}`);
    })

    this.listener.on('VERSION', (version: any) => {
      this.log.debug(`Gpsd release: ${version.release}`);
    })

  }

  /**
   * Concrete implementation of the abstract method.
   * @param name The name of the sensor.
   * @returns raw value from fan (speed in rpm)
   */
  async readRaw(): Promise<number | undefined> {
    return this.readSpeed()
  }

  /**
   * Concrete implementation of the abstract method.
   * @returns delay between reads (seconds)
   */
  async getDelay() : Promise<number> {

    if (this.location && this.location.speed > 3.0) {
      return 1
    } else {
      return this.config.rate
    }
  }

  async handleGpsData(data: any) : Promise<void> {
    if (data.mode && data.mode < 2) {  // need at least 2D fix
      return
    }

    // set current location
    this.location = data
  }

  async readSpeed() : Promise<number | undefined>{

    if (! this.location) {
      return undefined
    }

    // write out gps location
    try {
      const filePath = this.file
  
      await fs.appendFile(filePath, `${JSON.stringify(this.location)}\n`, 'utf8');
    } catch (err: any) {
      this.log.error(err.message)
    }

    // report the fastest speed
    if (this.sensorMaximum && this.location.speed > this.sensorMaximum) {
      this.log.debug(`sensorMaximum: ${this.sensorMaximum} location.speed: ${this.location.speed}`)
      clearInterval(this.delayMaxSpeedReport)
      this.delayMaxSpeedReport = setTimeout(4 * 1000, this.location.speed).then(async (speed: number) => {
        this.tts(`The fastest speed is ${Math.round(speed * 10) / 10} mph`)
      })
    }

    // return the speed
    return this.location.speed
  }

  connectToGps() : void {
    this.log.debug('connecting...')
    this.listener.connect( () => {
      this.log.info('Connected to gpsd')
      // TODO: need to unwatch at some point
      this.listener.watch({ class: 'WATCH', json: true, nmea: false })

      this.listener.device()
    })
  }

  async close(): Promise<void> {
    this.log.debug('disconnecting from gpsd...')
    this.closing = true
    this.listener.disconnect()

    super.close()
  }

}