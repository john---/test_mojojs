import { promises as fs } from 'fs'
import Sensor from './sensor.js'
import { Logger } from '@mojojs/core'
import Pg from '@mojojs/pg'
import { EventEmitter } from 'node:stream'

export default class Sys extends Sensor {

  constructor (config: any, volume: number, source: string, pg: Pg, events: EventEmitter<[never]>, log: Logger) {

    super(config, volume, source, pg, events, log)
  }

  /**
   * Concrete implementation of the abstract method.
   * @param name The name of the sensor.
   * @returns raw value from fan (speed in rpm)
   */
  async readRaw(): Promise<number | undefined> {
    return this.readCpuTemp()
  }

  /**
   * Concrete implementation of the abstract method.
   * @returns delay between reads (seconds)
   */
  async getDelay() {
    return this.config.rate
  }

  async readCpuTemp() {
    try {
      const data = await fs.readFile(this.config.params.file)
      return (Number(data.toString()) / 1000 * 1.8 + 32)
    } catch (err) {
      console.log(err)
    }
    return undefined
  }

}