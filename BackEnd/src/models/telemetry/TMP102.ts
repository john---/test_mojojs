
import { openPromisified } from 'i2c-bus'
import Sensor from './sensor.js'
import { Logger } from '@mojojs/core'
import Pg from '@mojojs/pg'
import { EventEmitter } from 'node:stream'

const REG = {
  TEMP: 0x00,    // R
  CONFIG: 0x01,  // R/W
  T_LOW: 0x02,   // R/W
  T_HIGH: 0x03,  // R/W
}

export default class TMP102 extends Sensor {

  constructor (config: any, volume: number, source: string, pg: Pg, events: EventEmitter<[never]>, log: Logger) {

    super(config, volume, source, pg, events, log)
  }

  /**
   * Concrete implementation of the abstract method.
   * @param name The name of the sensor.
   * @returns raw value from fan (speed in rpm)
   */
    async readRaw(): Promise<number | undefined> {
      return this.readTempFahrenheit()
    }
  
    /**
     * Concrete implementation of the abstract method.
     * @returns delay between reads (seconds)
     */
    async getDelay() {
      return this.config.rate
    }

  /**
   * Returns the temperature in degrees Celsius.
   *
   * @returns The temperatire in degrees Celsius
   *
   */
  async readTemp() {

    const twosComp = (val: number, bits: number) => {
      if ((val & (1 << (bits - 1))) != 0) {
          val = val - (1 << bits)
      }
      return val
    }

    const toTemp = (rawData: number) => {
      var lo = rawData & 0xff00
      lo = lo >> 8     // data is read little endian so swap bytes

      const hi = rawData & 0x00ff

      //const hilo = hi * 256 + lo

      //var t = hilo >> 4  // hardcoding the default instead of using chip config

      var t = (hi << 4) | (lo >> 5)

      // Convert to 2s complement (temperatures can be negative)
      t = twosComp(t, 12)

      // if ((hi | 0x7F) == 0xFF) { // negative temperature
      //   t = ~t +1
      //   t &= 0xFFF
      //   t *= -1
      // }

      return t * 0.0625
    }

    const bus = await openPromisified(this.config.params.bus)
    if (bus) {
      const i2c0 = await bus.readWord(this.config.params.addr, REG.TEMP)
      bus.close()
      if (i2c0) {
        const vShunt = toTemp(i2c0)
        return vShunt
      }
    }
  }

  async readTempFahrenheit() {
    const temp = await this.readTemp()

    return temp! * 1.8 + 32
  }

//   /**
//    * Returns the current in millamps.
//    *
//    * @returns The current in mA
//    *
//    */
//   async readCurrent() {
//     const shuntVoltage = await this.readShuntVoltage()
//     // 0.1 Ohm resistor.  TODO: config ina219 and use current register
//     return shuntVoltage! / 0.1 / 1000
//   }

//     /**
//    * Returns the bus voltage reading scaled integer in millivolts and other relevant info.
//    *
//    * @returns The shunt voltage in mV and OVF, CNVR flags
//    *
//    */  
//   async readBusVoltage() {

//     const toVbus = (rawData: number) => {
//       const beData = (rawData >> 8) + ((rawData & 0xff) << 8)

//       const ovf  = ( beData & 1<<0 )
//       const cnvr = ( beData & 1<<1 )
//       const vraw = beData >> 3

//       return [vraw * 4, cnvr, ovf]
//     }

//     const bus = await openPromisified(this.config.params.bus)
//     if (bus) {
//       const i2c0 = await bus.readWord(this.config.params.addr, REG.VBUS)
//       bus.close()
//       if (i2c0) {
//         const values = toVbus(i2c0)
//         return values
//       }
//     }

//   }

//   /**
//    * Returns the bus voltage reading scaled integer in volts.
//    *
//    * @returns The shunt voltage in mV
//    *
//    */  
//   async readVoltage() {
//     const busVoltage = await this.readBusVoltage()
//     return busVoltage![0] / 1000
//   }
}