import OpenWeatherAPI, { Coordinates } from "openweather-api-node"
import Sensor from './sensor.js'
import { Logger } from '@mojojs/core'
import Pg from '@mojojs/pg'
import { EventEmitter } from 'node:stream'

export default class Weather extends Sensor {

  weather: OpenWeatherAPI
  temperature: number | undefined
  upcomingRain: number

  constructor (config: any, volume: number, source: string, pg: Pg, events: EventEmitter<[never]>, log: Logger) {

    super(config, volume, source, pg, events, log)

    let coordinates: Coordinates = { lat: Number(process.env.LAT), lon: Number(process.env.LON) }

    this.weather = new OpenWeatherAPI({
      key: process.env.KEY,
      coordinates: coordinates,
      units: "imperial"
    })

    this.upcomingRain = 0  // start out with no upcoming rain
  }

  /**
   * Concrete implementation of the abstract method.
   * @param name The name of the sensor.
   * @returns raw value from fan (speed in rpm)
   */
  async readRaw(): Promise<number | undefined> {
    if (!this.weather) {
      return undefined
    }

    this.temperature = (await this.weather.getCurrent()).weather.temp.cur

    // check for upcoming rain
    const forecast = await this.weather.getMinutelyForecast(20)
    // forecast[5].weather.rain = 3
    this.upcomingRain = forecast.findIndex( (element) => element.weather.rain > 0 ) + 1
    if (this.upcomingRain > 0) {
      this.tts(`Rain in ${this.upcomingRain} minute${this.upcomingRain !== 1 ? 's' : ''}`)
    }
  
    return this.temperature
  }

  /**
   * Concrete implementation of the abstract method.
   * @returns delay between reads (seconds)
   */
  async getDelay() {
    if (this.upcomingRain > 0) {  // rain is coming so check more often
      return 2 * 60
    } else {
      return this.config.rate
    }
  }

}
