import Pg from "@mojojs/pg"
import { EventEmitter } from 'node:stream'
import { exec } from 'child_process'
import { temporaryFile } from 'tempy'
import { Datum } from '../types.js'
import { Logger } from "@mojojs/core"
import { setTimeout } from "node:timers/promises"


export function setSubscription(state: boolean): void {

  // TODO: need to figure out how to get detail to be sent/stooped to a particular client

  Sensor.subscribe = state
}

export default abstract class Sensor {

  config: any
  volume: number
  source: string
  pg: Pg
  events: EventEmitter
  log: Logger
  device: any
  ac: any
  //signal: any
  sensorValue: number | undefined
  previousSensorValue: number | undefined
  lastNotificationValue: number
  sensorMinimum: number | undefined
  sensorMaximum: number | undefined
  static subscribe: boolean

  constructor(config: any, volume: number, source: string, pg: Pg, events: EventEmitter, log: Logger) {

    this.config = config
    this.volume = volume
    this.source = source
    this.pg = pg
    this.events = events
    this.log = log
    this.sensorValue = 0
    this.lastNotificationValue = 0
    this.ac = new AbortController()

    this.log.debug(`created: ${config.name}`)
  }

  abstract readRaw(name: string): Promise<number | undefined>
  abstract getDelay(): Promise<number>
  async setFanSpeed(speed: number): Promise<void> {
    // do nothing
  }

  async kickoff(): Promise<void> {
    await this.delayUntilNextCycle()
  }

  async delayUntilNextCycle(): Promise<void> {

    //this.log.debug(`the new loop for ${this.config.type}`)

    await this.sensorCycle()

    // instead of config rate get what the sensor wants for rate
    const delay = await this.getDelay()

    await setTimeout(delay * 1000, undefined, { signal: this.ac.signal })
    .then(async () => {
      await this.delayUntilNextCycle()
    })
    .catch((err) => {
      // if (err.name === 'AbortError')
      //   console.error('The timeout was aborted')
    })

  }

  async sensorCycle(): Promise<void> {
    try {
      await this.read()
      await this.record()
      await this.publish()
      await this.announce()
    }
    catch (e: any) {
      await this.publish()
      this.log.error(e)
    }
  }

  async read(): Promise<void> {

    const rawValue = await this.readRaw(this.config.name)
    // const rawValue = await (this.config.reader as () => Promise<number>)()

    if (typeof rawValue === 'undefined') {
      this.sensorValue = undefined
      throw new Error(`undefined sensor value for ${this.config.type}`)
    }

    this.sensorValue = Math.round(rawValue * 10) / 10

    this.log.debug(`value (${this.config.name}): ${this.sensorValue}`)

    if (typeof this.sensorMinimum == 'undefined' || this.sensorValue < this.sensorMinimum) {
      this.sensorMinimum = this.sensorValue
      this.emitUpdate('min', this.sensorMinimum)
    } else if (typeof this.sensorMaximum == 'undefined' || this.sensorValue > this.sensorMaximum) {
      this.sensorMaximum = this.sensorValue
      this.emitUpdate('max', this.sensorMaximum)
    }
  }

  async record(): Promise<void> {
    // write stuff to DB
    await this.pg.query`insert into sensor_history (input, value) values (${this.config.name}, ${this.sensorValue})`
  }

  async publish(): Promise<void> {
    if (this.sensorValue != this.previousSensorValue || this.previousSensorValue == undefined) {
      this.emitUpdate('value', this.sensorValue)
    }

    this.previousSensorValue = this.sensorValue
  }

  async tts(words: string): Promise<void> {
    const file = temporaryFile({ extension: 'wav' })
    const cmd = `echo ${words} | /usr/bin/text2wave -o ${file}`
    this.log.debug(cmd)

    exec(cmd, (error) => {
      if (error) {
        this.log.error(`error: ${error.message}`)
        return
      }

      var detail = {
        type: 'audio',
        source: this.source,
        volume: this.volume,
        file: file.replace('/tmp/', ''),  // on Linux, tmpy puts files in /tmp
        label: words,
      }

      this.events.emit(this.source, detail)
    })

  }

  async announce(): Promise<void> {

    if (!this.sensorValue) {
      return
    }

    if (Math.abs(this.sensorValue - this.lastNotificationValue) <= this.config.notify.threshold) {
      return
    }

    const interpolate = (str: string, obj: any) => str.replace(
      /\${([^}]+)}/g,
      (_, prop) => obj[prop]
    )

    // echo "%s" | /usr/bin/text2wave -o %s
    // notify: { threshold: 200, phrase: 'Power consumption at %.1f milliamps' }
    const round = this.config.notify.round || 0
    const words = interpolate(this.config.notify.phrase, { value: String(this.sensorValue.toFixed(round)) })
    await this.tts(words)

    this.lastNotificationValue = this.sensorValue
  }

  async emitUpdate(field: string, value: number | undefined): Promise<void> {
    if (! Sensor.subscribe && this.config.name != 'cpu_temp') {  // user not on the telemetry page or needed for termal control
      return
    }

    // sensor may not have valid value (e.g. location service not connected to gpsd)
    let reportValue: number | string
    if (value !== undefined) {
      reportValue = value
    } else {
      reportValue = 'Unknown'
    }

    this.events.emit(this.source, {
      'type': 'update',
      metrics: [{ 'input': this.config.name, [field]: reportValue }]
    })
  }

  async getMinMax(): Promise<Datum | undefined> {
    const results = await this.pg.query<Datum>`select input, 'Pending...' as value, min(value), max(value) from sensor_history where input = ${this.config.name} and recorded_at > now() - interval '3 days' group by input`
    if (results.length === 1) {
      this.sensorMinimum = results[0].min
      this.sensorMaximum = results[0].max
      results[0].value = this.sensorValue
      return results[0]
    }

    return undefined
  }

  isFan(): boolean {
    return (this.config.name as string).includes('fan')
  }

  async close(): Promise<void> {
    this.log.debug(`closing ${this.config.name}`)
    this.ac.abort()
  }

}