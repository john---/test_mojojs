import Pg from "@mojojs/pg"
import { EventEmitter } from 'node:stream'
import { Logger } from "@mojojs/core"
import Sensor from './sensor.js'
import INA219 from './INA219.js'
import TMP102 from './TMP102.js'
import ADT7470 from './ADT7470.js'
import Sys from './Sys.js'
import Location from './Location.js'
import Weather from './Weather.js'

const sensorMap: { [id: string]: any } = {
  'INA219': INA219,
  'TMP102': TMP102,
  'ADT7470': ADT7470,
  'Sys': Sys,
  'Location': Location,
  'Weather': Weather
}

export class Factory {

  static getSensor(config: any, volume: number, source: string, pg: Pg, events: EventEmitter, log: Logger): Sensor {
    return new (sensorMap[config.type])(config, volume, source, pg, events, log)
  }

}