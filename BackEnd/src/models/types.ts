// These are pass through to have one less import elsewhere
import { Scanner } from './scanner.js'
import { Telemetry } from './telemetry.js'
import { Administration } from './administration.js'
import { Leaderboard } from './leaderboard.js'
export { Scanner, Telemetry, Administration, Leaderboard }

// add feature to featureMap and features array
export const featureMap: { [id: string]: any } = {}
featureMap['scanner'] = Scanner
featureMap['telemetry'] = Telemetry
featureMap['administration'] = Administration
featureMap['leaderboard'] = Leaderboard

const features = ['scanner', 'telemetry', 'administration', 'leaderboard'] as const
export type Feature = typeof features[number]

export type StatusMsg =   // this is generic across all features
  {
    type: 'status'
    detail: 'green'
  }

export type ErrorMsg =
  {
    type: 'error'
    detail: string
  }

// Scanner messages

export type Bank =
  {
    bank: string
  }

export type ScannerAudioMsg =
  {
    type: 'audio'
    file: string
    source: string
    duration: number
    detected_as: string
    prediction: {
      V: number
      D: number
      S: number
    }
    freq: number
    freq_key: number
    label: string
    bank: string
    pass: boolean
    xmit_key: number
    class: string
    volume: number
    url: string
    replayed: boolean
  }

export type ScannerUpdateMsg =
  {
    type: 'update'
    xmit_key: number
    freq_key: number
    field: ScannerUpdateable
    value: string
  }

type ScannerUpdateable = 'label' | 'bank' | 'class'
export type ScannerChangeMsg =  // not sure if this needs to be exported
  {
    type: 'change'
    xmit_key: number
    freq_key: number
    field: ScannerUpdateable
    value: string
  }

export type ScannerConfigMsg =
  {
    type: 'config'
    gain: number
    banks: Array<string>
  }

export type ScannerMsg = ScannerUpdateMsg | ScannerChangeMsg | ScannerConfigMsg | StatusMsg | ErrorMsg

// Telemetry messages

export type TelemetryAudioMsg =
  {
    type: 'audio'
    file: string
    source: string
    duration: number
    label: string
    volume: number
    url: string
    replayed: boolean
  }

  export type Datum =
  {
    input: string
    value?: number
    min?: number
    max?: number
  }

export type TelemetryUpdateMsg =  // not sure if this needs to be exported
  {
    type: 'update'
    metrics: Array<Datum>
    // input: string
    // value: number
    // min: number
    // max: number
  }

export type SubscribeMsg =
  {
    type: 'subscribe' | 'unsubscribe'
    //channel: string
  }

export type TelemetryConfigMsg =
  {
    type: 'config'
  }

export type TelemetryMsg = TelemetryAudioMsg | TelemetryUpdateMsg |
            TelemetryConfigMsg | StatusMsg | SubscribeMsg | ErrorMsg

// Administration messages

// type AdministrationCommandMsg =
//   {
  
//   }

export type AdministrationConfigMsg =
  {
    type: 'config'
    system: Array<string>
  }

export type AdministrationCommandMsg =
  {
    type: 'command'
    command: string
  }

export type AdministrationMsg = AdministrationConfigMsg | AdministrationCommandMsg | ErrorMsg | StatusMsg

// Leaderboard messages

export type LeaderboardConfigMsg =
 {
  type: 'config'
  domain: 'sessions' | 'positions'
 }

export type LeaderboardUpdateMsg =
{
  type: 'update'
  domain: 'sessions' | 'positions'
  data: Array<any>

}

export type LeaderboarMsg = LeaderboardConfigMsg | LeaderboardUpdateMsg | ErrorMsg | StatusMsg

export type FeatureMsg = ScannerMsg | TelemetryMsg // | LeaderboardMsg
export type AudioMsg = ScannerAudioMsg | TelemetryAudioMsg // | LeaderboardAudioMsg  
