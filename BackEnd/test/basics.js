import { app } from '../lib/index.js'
import Sensor from '../lib/models/telemetry/sensor.js'
import {EventEmitter} from 'node:events'
import t from 'tap'

import Pg from "@mojojs/pg"

import { strict as assert } from 'node:assert'
import { randomInt } from 'node:crypto'

import 'dotenv/config' // At least need to Open Weather API key

import { error } from 'node:console'

app.log.level = 'debug'

app.models.pg = new Pg(app.config.pg)  // used for pub/sub testing
app.models.pg.db().then(
  (result) => { app.models.db = result },
  (error) => { console.log(`Could not get a db connection: ${error}`) }
)

const isType = (expectedType, msg) => {
  const obj = JSON.parse(msg)
  if (obj.type !== expectedType) { throw new Error(`Not the expected type (expected: ${expectedType}): ${msg}`) }
}

t.test('Cart Dashboard', async t => {
  const ua = await app.newTestUserAgent({ tap: t })

  await t.test('Basic application testing', async () => {
    (await ua.getOk('/', { auth: 'test:account' })).statusIs(200)

    // await ua.websocketOk('/messages')
    // await ua.closeOk(4000, 'no feature supplied')

    // await ua.websocketOk('/messages?blah=scanner')
    // await ua.closeOk(4000, 'not closed with wrong param supplied')

    // await ua.websocketOk('/messages?feature=table')
    // await ua.closeOk(4000, 'not closed with wrong feature supplied')

    await ua.websocketOk('/messages?feature=scanner', { auth: 'test:account' })

    await ua.sendOk('{ "type" : "status" }', 'send a message')
    //console.log(await ua.messageOk())
    assert.equal(await ua.messageOk(), '{"type":"status","detail":"green"}', 'receive response to ping')

    await ua.sendOk('{ "type" : "blah" }')
    isType('error', await ua.messageOk())

    await ua.sendOk('{ "blah" : "status" }')
    isType('error', await ua.messageOk())

    await ua.sendOk('{ "scanner":"status", "leaderboard":"status"}')
    isType('error', await ua.messageOk())

    try {
      const msg = '{"type":"audio","uri":"blah","detected_as":"V"}'
      app.models.db.notify('scanner', msg);   // test message
      assert.equal(await ua.messageOk(), msg)
    } finally {  // this doesn't allow exiting of test cases with failed test
      await app.models.db.release()
    }

    (await ua.getOk('/audio?file=test_file.wav&source=scanner')).statusIs(200)

    await ua.closeOk(4000)
    await ua.closedOk(4000)
  });

  await t.test('Scanner testing', async () => {
    (await ua.getOk('/', { auth: 'test:account' })).statusIs(200)

    await ua.websocketOk('/messages?feature=scanner', { auth: 'test:account' })

    await ua.sendOk('{ "type" : "config" }', 'config request')
    const response = await ua.messageOk()
    isType('config', response)
    const obj = JSON.parse(response)
    assert.ok(obj.banks.length > 0)

    await ua.sendOk('{ "type" : "change", "xmit_key": "-1", "freq_key": "3973" }', 'no "value" field')
    isType('error', await ua.messageOk())

    await ua.sendOk('{ "type" : "change", "xmit_key": "538020", "freq_key": "3973", "value": "Label~" }', 'send invalid character')
    isType('error', await ua.messageOk())

    await ua.sendOk('{ "type" : "change", "xmit_key": "-1", "freq_key": "-1", "field": "label", "value": "thelabel" }', 'send invalid field')
    isType('error', await ua.messageOk())

    await ua.sendOk('{ "type" : "change", "xmit_key": "538020", "freq_key": "3973", "field": "wrong", "value": "thelabel" }', 'invalid field')
    isType('error', await ua.messageOk())

    // specifc database entries required from here

    // add records here and user with privileges needs to delete
    // delete from xmit_history where source = 'TESTER';
    // delete from freqs where source = 'TESTER';
    const freq = randomInt(500)
    let results = await app.models.pg.rawQuery(`insert into freqs(freq, label, bank, source) values ($1, $2, 'ATC', 'TESTER') returning freq_key`, freq, 'Something (' + randomInt(200) + ')')
    const freq_key = results[0].freq_key
    results = await app.models.pg.rawQuery(`insert into xmit_history(freq_key, source, file, duration, detect_voice, class) values ($1, 'TESTER', 'a_file.wav', '1 minute', true, 'U') returning xmit_key`, freq_key)
    const xmit_key = results[0].xmit_key
    // insert another frequency for unique key constraint test
    results = await app.models.pg.rawQuery(`insert into freqs(freq, label, bank, source) values ($1, $2, 'FRS', 'TESTER') returning freq_key`, freq, 'Something (' + randomInt(200) + ')')

    await ua.sendOk(`{ "type" : "change", "xmit_key": "${xmit_key}", "freq_key": "${freq_key}", "field": "label", "value": "thelabel" }`, 'change label')
    isType('update', await ua.messageOk())

    await ua.sendOk(`{ "type" : "change", "xmit_key": "${xmit_key}", "freq_key": "${freq_key}", "field": "bank", "value": "FRS" }`, 'change bank and cause unique key constraint issue')
    isType('error', await ua.messageOk())

    await ua.sendOk(`{ "type" : "change", "xmit_key": "${xmit_key}", "freq_key": "${freq_key}", "field": "bank", "value": "CB" }`, 'change bank')
    isType('update', await ua.messageOk())

    await ua.sendOk(`{ "type" : "change", "xmit_key": "${xmit_key}", "freq_key": "${freq_key}", "field": "class", "value": "U" }`, 'change class')
    isType('update', await ua.messageOk())

    // await app.models.db.release()

    await ua.closeOk(4000)
    await ua.closedOk(4000)
  });

  // app.config.telemetry.devices.TEST = { type: 'I2C', params: { bus: 0, addr: 0x40 } }

  await t.test('Telemetry testing', async () => {
    (await ua.getOk('/', { auth: 'test:account' })).statusIs(200)

    await ua.websocketOk('/messages?feature=telemetry', { auth: 'test:account' })

    // // check the initial transmission of metrics
    // response = await ua.messageOk()
    // console.log(response)
    // isType('update', response)
    // const obj = JSON.parse(response)
    // assert.ok(obj.metrics.length > 0)

  //   await ua.sendOk('{ "type" : "blah" }')
  //   isType('error', await ua.messageOk())

    await ua.sendOk('{ "type" : "config" }', 'config request')
    var response = await ua.messageOk()
    isType('config', response)

    // await ua.sendOk('{ "type" : "status" }', 'send a message')
    //assert.equal(await ua.messageOk(), '{"type":"status","detail":"green"}', 'receive response to ping')


    // // after 5 seconds should receive an update from a sensor
    // console.log('about to wait 1')
    // response = await ua.messageOk()
    // isType('update', response)
    // console.log('done waiting')

    // // after 5 seconds should receive an update from a sensor
    // console.log('about to wait 2')
    // response = await ua.messageOk()
    // isType('update', response)
    // console.log('done waiting')

    // // after 5 seconds should receive an update from a sensor
    // console.log('about to wait 3')
    // response = await ua.messageOk()
    // isType('update', response)
    // console.log('done waiting')

    // // after 5 seconds should receive an update from a sensor
    // console.log('about to wait 4')
    // response = await ua.messageOk()
    // isType('audio', response)
    // console.log('done waiting')

    // console.log('about to wait 5')
    // response = await ua.messageOk()
    // isType('audio', response)
    // console.log('done waiting')

    // console.log('about to wait 6')
    // response = await ua.messageOk()
    // isType('update', response)
    // console.log('done waiting')

    // update message sent after connection.  I am not sure why this doesn't get confused with "type" above
    // response = await ua.messageOk()
    // isType('update', response)
    // const obj = JSON.parse(response)
    // assert.ok(obj.metrics.length > 0)

    // after 5 seconds should receive an update from a sensor
    // console.log('about to wait')
    // response = await ua.messageOk()
    // isType('update', response)
    

    await ua.closeOk(4000)
    await ua.closedOk(4000)
  });

  await t.test('Leasderboard testing', async () => {
    (await ua.getOk('/', { auth: 'test:account' })).statusIs(200)

    await ua.websocketOk('/messages?feature=leaderboard', { auth: 'test:account' })

    await ua.sendOk('{ "type" : "config", "domain": "sessions" }', 'sessions request')
    var response = await ua.messageOk()
    isType('update', response)

    await ua.closeOk(4000)
    await ua.closedOk(4000)
  });

  await ua.stop()

  // app.models.events = new EventEmitter()
  // app.models.events.setMaxListeners(1000)

  // await t.test('Sensor class testing', async () => {
  //   for (const sensor of app.config.telemetry.sensors) {
  //     const sensorObj = new Sensor(sensor, app.config.telemetry.volume, 'telemetry', app.models.pg, app.models.events, app.log)
  //     const minmax = await sensorObj.getMinMax()
  //     t.ok((minmax.min <= minmax.value), 'min less or equal to value')
  //     t.ok((minmax.max >= minmax.value), 'max greater or equal to value')

  //     await sensorObj.close()
  //   }

  // })
  // app.models.events = new EventEmitter()
  // app.models.events.setMaxListeners(1000)

  // await t.test('Telemetry class testing', async () => {
  //   const telemetry = new Telemetry(app.models.pg, app.config, app.log, app.models.events)

  //   // maybe test Telemetry or Sensor
    

  //   telemetry.close()
  // })

});
