
import * as fs from 'fs';
import {app} from '../lib/index.js'
import Pg from "@mojojs/pg"
const result = JSON.parse(fs.readFileSync('../test_data/xmits.json'));

const pg = new Pg(app.config.pg)
const db = await pg.db()

let myPromise = () => new Promise((resolve) => {
  setTimeout(function(){
    resolve('Count')
  }, 3000)
})

for (let index = 0; index < (Array.from(result).length); index++) {
  await myPromise()
  console.log(result[index])
  db.notify('scanner', JSON.stringify(result[index]))   
}

 db.release()