import Pg from "@mojojs/pg"

const pg = new Pg('postgres://script@db:5432/cart')

wrapper()

async function wrapper() {
  const config = await getConfig()
  console.log(config)
}

async function getConfig(){
  let banks = await getBanks()
  let banks2 = await getBanks()
  let config = { banks: banks, banks2: banks2 }
  //console.log(config)
  return config
}

function getBanks() {
  const results = pg.query`select distinct(bank) from freqs order by bank asc`
  .then( (banks) => { return banks.map( (item) => { return item.bank })})

  return results
 }
