import { origin } from './urlhelper'
import { Feature } from '../../../BackEnd/src/models/types'
import { Notify } from 'quasar'

export default class CommonSocket {

  ws: WebSocket
  waitForServer: number | undefined

  constructor(feature: Feature) {

    // TODO:  take a look at this: https://vueuse.org/core/useWebSocket/
    this.ws = new WebSocket(`${origin('ws')}/messages?feature=${feature}`)

    this.ws.addEventListener('close', (event) => {
      error(`websocket closed: ${event.reason}`)
    })

    //const pingInterval = setInterval( () => {
    setInterval(() => {
      this.ws.send(JSON.stringify({ type: 'status' }))
      console.log('just requested status')
      this.waitForServer = window.setTimeout(function () {
        error('server not available or problem on server!');
      }, 2000)
    }, 45000)

    // load error audio
    // add listener for status
    // timeout for not getting status
    //   play audio if not getting responses
    this.ws.addEventListener('message', (event) => {
      const msg = JSON.parse(event.data)
      switch (msg.type) {
        case 'status':
          if (msg.detail == 'green') {
            clearTimeout(this.waitForServer)
          }
          break
      }
    })

    const error = (error: string) => {
      Notify.create(error)
      console.log(error)
    }
  }
}