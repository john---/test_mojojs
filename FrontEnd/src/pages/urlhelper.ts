
type Protocols = 'http' | 'ws'

export const origin = (protocol: Protocols) => {

  const host = process.env.DEV ? process.env.API : window.location.host

  return`${protocol}://${host}`
}