import { RouteRecordRaw } from 'vue-router'
import MainLayout from 'layouts/MainLayout.vue'
import ScannerPage from 'pages/ScannerPage.vue'
import LeaderboardPage from 'pages/LeaderboardPage.vue'
import TelemetryPage from 'pages/TelemetryPage.vue'
import AdministrationPage from 'pages/AdministrationPage.vue'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: MainLayout,
    children: [
      {
        path: 'scanner',
        component: ScannerPage
      },
      {
        path: 'leaderboard',
        component: LeaderboardPage
      },
      {
        path: 'telemetry',
        component: TelemetryPage
      },
      {
        path: 'administration',
        component: AdministrationPage
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
