import { fileURLToPath, URL } from 'node:url'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

const isDev = process.env.MOJO_MODE === 'development'
process.env.NODE_ENV = 'development'

// https://vitejs.dev/config/
export default defineConfig({
  base: '/static/',
  plugins: [
    vue(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    proxy: {
      '/backend': {
        target: 'http://localhost:3000/'
      },
    }
  },
  build: {
   emptyOutDir: true,  // needed when writing directly to mojo public dir
   rollupOptions: {
    output: {
      entryFileNames: isDev ? '[name].development.js' : '[name].[hash].js',
      dir: '../BackEnd/public',
      format: 'iife'
    }
   }
 }
})
