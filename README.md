# Cart Dashboard
This application provides a consolidated dashboard for a mobile system implemented by the user.  It is a highly specific application and not intended for general use.

## Current functionality
- Monitor radio transmissions
- Generate and monitor telementry data related to the mobile system
  - Current draw, cpu temperature, fan speeds, etc.
  - GPS speed and location
  - Ambient weather conditions including temperture and upcoming precipitation
- Control fan speeds to keep cpu temperature within a specified range

## Dependencies
The application needs to be installed on a linux computer.  It requires an Internet connection.  It also needs to have a software defined radio (SDR) that can be controlled by software.  

System requirements:
- Internet connection
- Postgresql database
- Python3
- Node.js
- SDR hardware
- [Ham2mon](https://github.com/john-/ham2mon)
- API token for [Open Weather](https://openweathermap.org/api)

There are other dependencies not yet listed.